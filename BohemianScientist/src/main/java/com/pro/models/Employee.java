package com.pro.models;

public class    Employee {
    // ([0]First Name) ([1] Last Name) ([2] SSN) ([3] Phone Number) ([4] Email) ([5] password) ([6] is a Manager)
    private int ID;
    private String firstName;
    private  String lastName;
    private  int SSN;
    private  String phoneNumber;
    private String email;
    private  String password;
    private boolean isManager;

    public Employee() {
    }


    public Employee(int ID, String firstName, String lastName, int SSN, String phoneNumber, String email, String password, boolean isManager) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.SSN = SSN;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.isManager = isManager;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(int SSN) {
        this.SSN = SSN;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean manager) {
        isManager = manager;
    }
}


