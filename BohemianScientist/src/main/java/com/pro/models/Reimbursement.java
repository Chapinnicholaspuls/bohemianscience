package com.pro.models;

public class Reimbursement {
    int id;
    int emp_id;
    String type;
    double amount;
    String description;
    boolean is_active;
    boolean is_approved;

    public Reimbursement(){
        id = 0;
        emp_id = 0;
        type = "";
        amount = 0;
        description = "";
        is_active = true;
        is_approved = false;
    }

    public Reimbursement(int id, int emp_id, String type, double amount, String description, boolean is_active, boolean is_approved){
        this.emp_id = emp_id;
        this.is_approved = is_approved;
        this.is_active = is_active;
        this.id = id;
        this.type = type;
        this.description = description;
        this.amount = amount;
    }

    public int getId() { return id; }

    // TODO Verify that set values are correct
    public void setId(int id) { this.id = id; }

    public int getEmp_id() { return emp_id; }

    public void setEmp_id(int emp_id) { this.emp_id = emp_id; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public double getAmount() { return amount; }

    public void setAmount(double amount) { this.amount = amount; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public boolean getIs_active() { return is_active; }

    public void setIs_active(boolean is_active) { this.is_active = is_active; }

    public boolean getIs_approved() { return is_approved; }

    public void setIs_approved(boolean is_approved) { this.is_approved = is_approved; }
}
