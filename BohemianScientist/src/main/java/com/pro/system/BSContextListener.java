package com.pro.system;

import com.pro.data.EmployeeRepositoryJDBC;
import com.pro.data.PostgresConnectionManager;
import com.pro.data.ReimbursementRepositoryImpl;
import com.pro.services.UserAuthenticationImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class BSContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext  context = sce.getServletContext();
        ConnectionManager manager = new PostgresConnectionManager();
        manager.init();

        EmployeeRepositoryJDBC ERJDBC = new EmployeeRepositoryJDBC(manager);
        ReimbursementRepositoryImpl REJDBC = new ReimbursementRepositoryImpl(manager);
        UserAuthenticationImpl UAS = new UserAuthenticationImpl(ERJDBC);

        context.setAttribute("ERJDBC", ERJDBC);
        context.setAttribute("REJDBC", REJDBC);
        context.setAttribute("UAS", UAS);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
