package com.pro.data;

import com.pro.models.Reimbursement;

import java.util.List;

public interface ReimbursementRepository extends Repository<Reimbursement> {

    // Used to get a list of all active/(waiting for review) Reimbursements for managers
    List<Reimbursement> findAllActive();

    // Used to get a list of all reviewed reimbursements
    List<Reimbursement> findAllInactive();

    // used to get a list of all Reimbursements for a specific employee, both manager and employee should use
    List<Reimbursement> findAllByUserID(int id);
}
