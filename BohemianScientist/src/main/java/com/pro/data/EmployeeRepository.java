package com.pro.data;

import com.pro.models.Employee;

import java.util.List;

public interface EmployeeRepository extends Repository<Employee> {

    // Will be used to get a list of all employees
    List<Employee> findAll();
    // Find employee by their email
    Employee findByEmail(String email);
}
