package com.pro.data;

import com.pro.models.Employee;
import com.pro.system.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeRepositoryJDBC implements EmployeeRepository {

    private final ConnectionManager manager;
    public EmployeeRepositoryJDBC(ConnectionManager manager) {
        this.manager = manager;
    }

    @Override
    public List<Employee> findAll() {
        // ([0]ID)([1]First Name) ([2] Last Name) ([3] SSN) ([4] Phone Number) ([5] Email) ([6] password) ([7] is a Manager)
        String SQLStatement = "Select id, first_name, last_name, ssn, phone, email, password, is_manager from employee";
        try(Connection conn = manager.getConnection()){
            PreparedStatement PS = conn.prepareStatement(SQLStatement);
            ResultSet RS = PS.executeQuery();
            List returnList = new ArrayList();
            while (RS.next()){
                int ID = RS.getInt(0);
                String firstName = RS.getString(1);
                String lastName = RS.getString(2);
                int SSN = RS.getInt(3);
                String phone = RS.getString(4);
                String email = RS.getString(5);
                String password = RS.getString(6);
                boolean isManager = RS.getBoolean(7);
                Employee toAdd = new Employee(ID, firstName, lastName, SSN, phone, email, password, isManager);
                returnList.add(toAdd);

            }
            return  returnList;

        }catch (SQLException e) {
            System.out.println("We broke at the connection manager, boss!");
        }
        return null;
    }

    @Override
    public Employee findByEmail(String email) {
        String SQLStatement = "Select id, first_name, last_name, ssn, phone, email, password, is_manager from employee where email = ?";
        try(Connection conn = manager.getConnection()){
            PreparedStatement PS = conn.prepareStatement(SQLStatement);
            PS.setString(1, email);
            ResultSet RS = PS.executeQuery();
            Employee toReturn = new Employee();
            while (RS.next()){
                toReturn.setID(RS.getInt(0));
                toReturn.setFirstName(RS.getString(1));
                toReturn.setLastName(RS.getString(2));
                toReturn.setSSN(RS.getInt(3));
                toReturn.setPhoneNumber(RS.getString(4));
                toReturn.setEmail(RS.getString(5));
                toReturn.setPassword(RS.getString(6));
                toReturn.setManager(RS.getBoolean(7));
                break;
            }
            return toReturn;

        }catch (SQLException e) {
            System.out.println("We broke at the connection manager, boss!");
        }
        return null;
    }

    @Override
    public void Create(Employee obj) {

    }

    @Override
    public void update(Employee emp) {
        String SQLStatement = "update employee set first_name = ?,last_name = ?, phone = ?, 'password' = ? WHERE  id = ?";
        try (Connection conn = manager.getConnection()) {
            PreparedStatement PS = conn.prepareStatement(SQLStatement);
            PS.setString(1, emp.getFirstName());
            PS.setString(2, emp.getLastName());
            PS.setString(3, emp.getPhoneNumber());
            PS.setString(4, emp.getPassword());
            PS.setInt(5, emp.getID());

            }catch(SQLException e){
            System.out.println("We broke at the connection manager, boss!");
            }
    }


    @Override
    public Employee findByID(int id) {
        String SQLStatement = "Select id, first_name, last_name, ssn, phone, email, password, is_manager from employee where id = ?";
        try(Connection conn = manager.getConnection()){
            PreparedStatement PS = conn.prepareStatement(SQLStatement);
            PS.setString(1, String.valueOf(id));
            ResultSet RS = PS.executeQuery();
            Employee toReturn = new Employee();
            while (RS.next()){
                toReturn.setID(RS.getInt(0));
                toReturn.setFirstName(RS.getString(1));
                toReturn.setLastName(RS.getString(2));
                toReturn.setSSN(RS.getInt(3));
                toReturn.setPhoneNumber(RS.getString(4));
                toReturn.setEmail(RS.getString(5));
                toReturn.setPassword(RS.getString(6));
                toReturn.setManager(RS.getBoolean(7));
            }
            return toReturn;

        }catch (SQLException e) {
            System.out.println("We broke at the connection manager, boss!");
        }
        return null;
    }
}
