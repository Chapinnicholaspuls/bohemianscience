package com.pro.data;

import com.pro.system.ConnectionManager;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.SQLException;

public class PostgresConnectionManager implements ConnectionManager {

    HikariDataSource ds;

    @Override
    public void init() {
        String configFile=ClassLoader.getSystemClassLoader().getResource("db.properties").getPath();

         try {
            Class.forName(Driver.class.getName());

            HikariConfig config = new HikariConfig(configFile);
            ds = new HikariDataSource(config);

        } catch (ClassNotFoundException e) {
            // TODO Add a logger or exception throw here
        }
    }

    @Override
    public Connection getConnection() {
        if(ds != null) {
            try {
                return ds.getConnection();
            } catch (SQLException e) {
                // TODO Add a logger or exception throw here
            }
        } else {
            try {
                return ds.getConnection();
            } catch (SQLException e) {
                // TODO Add a logger or exception throw here
            } catch (NullPointerException e) {
                // TODO Add a logger or exception throw here
            }
        }
        return null;
    }
}
