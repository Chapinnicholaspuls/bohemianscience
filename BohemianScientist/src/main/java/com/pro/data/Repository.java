package com.pro.data;

public interface Repository<T> {
    // will be used to create reimbursements, could be used for creating employees
    void Create(T obj);
    // will be used to update both employees and reimbursements
    void update(T obj);
    // will be used to find an employee as well as pull up specific reimbursement information
    T findByID(int id);
}
