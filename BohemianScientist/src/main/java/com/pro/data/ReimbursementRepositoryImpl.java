package com.pro.data;

import com.pro.models.Reimbursement;
import com.pro.system.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementRepositoryImpl implements ReimbursementRepository {
    private final ConnectionManager manager;

    public ReimbursementRepositoryImpl(ConnectionManager conMan){ manager = conMan; }

    // TODO add a date column to table and queries
    @Override
    public List<Reimbursement> findAllActive() {
        try(Connection c = manager.getConnection()) {
            String sql = "select reim_id, emp_id, reimbursement_type, amount, description, is_approved, is_active " +
                    "from reimbursements " +
                    "where is_active = true;";

            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            return runResults(rs);

        } catch (SQLException e) {
            // TODO Add a logger and/or exception throw here
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Reimbursement> findAllInactive() {
        try(Connection c = manager.getConnection()) {
            String sql = "select reim_id, emp_id, reimbursement_type, amount, description, is_approved, is_active " +
                    "from reimbursements " +
                    "where is_active = false;";

            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            return runResults(rs);
        } catch (SQLException e) {
            // TODO Add a logger and/or exception throw here
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private List<Reimbursement> runResults(ResultSet rs){
        List<Reimbursement> reimbursements = new ArrayList<>();

        try {
            while (rs.next()) {
                Reimbursement r = new Reimbursement();
                r.setId(rs.getInt("reim_id"));
                r.setEmp_id(rs.getInt("emp_id"));
                r.setType(rs.getString("reimbursement_type"));
                r.setAmount(rs.getDouble("amount"));
                r.setIs_active(rs.getBoolean("is_active"));
                r.setIs_approved(rs.getBoolean("is_approved"));
                r.setDescription(rs.getString("description"));

                reimbursements.add(r);
            }
        }catch(SQLException e){
            // TODO add a logger and/or exception throw here
        }
        return reimbursements;
    }

    @Override
    public List<Reimbursement> findAllByUserID(int id) {
        try(Connection c = manager.getConnection()) {
            String sql = "select reim_id, emp_id, reimbursement_type, amount, description, is_approved, is_active " +
                    "from reimbursements " +
                    "where emp_id = ?;";

            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet rs = s.executeQuery();

            return runResults(rs);
        } catch (SQLException e) {
            // TODO Add a logger and/or exception throw here
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public void Create(Reimbursement obj) {
        try(Connection c = manager.getConnection()) {
            String sql = "insert into reimbursements(emp_id, reimbursement_type, amount, description) " +
                    "values(?, ?, ?, ?);";

            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, obj.getEmp_id());
            s.setString(2, obj.getType());
            s.setDouble(3, obj.getAmount());
            s.setString(4, obj.getDescription());
            s.execute();

        } catch (SQLException e) {
            // TODO Add a logger and/or exception throw here
            e.printStackTrace();
        }
    }

    @Override
    public void update(Reimbursement obj) {
        try(Connection c = manager.getConnection()) {
            String sql = "update reimbursements set is_active = ?, is_approved= ? where reim_id = ?;";

            PreparedStatement s = c.prepareStatement(sql);
            s.setBoolean(1, obj.getIs_active());
            s.setBoolean(2, obj.getIs_approved());
            s.setInt(3, obj.getId());

            s.executeUpdate();

        } catch (SQLException e) {
            // TODO Add a logger and/or exception throw here
            e.printStackTrace();
        }
    }

    @Override
    public Reimbursement findByID(int id) {
        Reimbursement reim = new Reimbursement();
        try(Connection c = manager.getConnection()) {
            String sql = "select reim_id, emp_id, reimbursement_type, amount, description, is_approved, is_active " +
                    "from reimbursements " +
                    "where reim_id = ?;";

            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet rs = s.executeQuery();
            while(rs.next()){
                reim.setIs_approved(rs.getBoolean("is_approved"));
                reim.setIs_active(rs.getBoolean("is_active"));
                reim.setAmount(rs.getDouble("amount"));
                reim.setType(rs.getString("reimbursement_type"));
                reim.setEmp_id(rs.getInt("emp_id"));
                reim.setDescription(rs.getString("description"));
                reim.setId(id);
                break;
            }
            return reim;
        } catch (SQLException e) {
            // TODO Add a logger and/or exception throw here
            e.printStackTrace();
        }
        return reim;
    }
}
