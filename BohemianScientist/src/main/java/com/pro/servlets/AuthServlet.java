package com.pro.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.pro.models.Employee;
import com.pro.services.UserAuthenticationImpl;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AuthServlet extends HttpServlet {
private UserAuthenticationImpl UAS;
private String username;
private String password;
private String JSON = "{}";
private boolean isAuth;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String serviceName = config.getInitParameter("UAS");
        UAS = (UserAuthenticationImpl) context.getAttribute(serviceName);

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getMethod();
        switch (method){
            case "POST":
                this.doPost(req, resp);
                break;
            case "GET":
                this.doGet(req,resp);
                break;
            default:
                break;
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
    //This Post requires  Header Values for username and password
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       username = req.getHeader("username");
       password = req.getHeader("password");
       Employee isauthenticated = UAS.authenticate(username,password);
       if (isauthenticated!=null){
            JSON = new ObjectMapper().writeValueAsString(isauthenticated);
       }
        resp.setStatus(200);
        resp.setContentType("application/json");
        resp.getWriter().write(JSON);
    }
    @Override
    public void destroy() {
        super.destroy();
    }


}

