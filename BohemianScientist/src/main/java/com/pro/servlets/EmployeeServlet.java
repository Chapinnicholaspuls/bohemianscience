package com.pro.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pro.data.EmployeeRepositoryJDBC;
import com.pro.data.ReimbursementRepositoryImpl;
import com.pro.models.Employee;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeServlet extends HttpServlet {
    EmployeeRepositoryJDBC ERJDBC;
    Employee user;
    List<Employee> listOfEmployees;
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String serviceName = config.getInitParameter("ERJDBC");
        ERJDBC = (EmployeeRepositoryJDBC) context.getAttribute(serviceName);

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getMethod();
        switch (method){
            case "POST":
                this.doPost(req, resp);
                break;
            case "GET":
                this.doGet(req,resp);
                break;
            default:
                break;
        }
    }
    //This Get requires Header Values for type and maybe email
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getHeader("type");
        if (type.equals("getOne")) {
            user = ERJDBC.findByEmail(req.getHeader("email"));
            String JSON = new ObjectMapper().writeValueAsString(user);
            resp.setContentType("application/json");
            resp.setStatus(200);
            resp.getWriter().write(JSON);

        }else if (type.equals("getAll")){
            listOfEmployees = ERJDBC.findAll();
            String JSON = new ObjectMapper().writeValueAsString(listOfEmployees);
            resp.setContentType("application/json");
            resp.setStatus(200);
            resp.getWriter().write(JSON);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }



    @Override
    public void destroy() {
        super.destroy();
    }


}