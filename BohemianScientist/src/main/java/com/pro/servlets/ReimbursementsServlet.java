package com.pro.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pro.data.ReimbursementRepository;
import com.pro.data.ReimbursementRepositoryImpl;
import com.pro.models.Reimbursement;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementsServlet extends HttpServlet {
    ReimbursementRepositoryImpl REJDBC;
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String serviceName = config.getInitParameter("REJDBC");
        REJDBC = (ReimbursementRepositoryImpl) context.getAttribute(serviceName);



    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getMethod();
        switch (method){
            case "POST":
                this.doPost(req, resp);
                break;
            case "GET":
                this.doGet(req,resp);
                break;
            default:
                break;
        }
    }


    //This Post requires  Header Values for type, userid, and reimid
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getHeader("type");
        int userid = Integer.parseInt(req.getHeader("userid"));
        int reimid = Integer.parseInt(req.getHeader("reimid"));
        List<Reimbursement> reimbursements=null;
        Reimbursement justNeo = null;
        int WHAMEN = 0;
        switch(type) {
            case "active": reimbursements = REJDBC.findAllActive();


                break;
            case "inactive": reimbursements = REJDBC.findAllInactive();

                break;

            case "byuserid": reimbursements = REJDBC.findAllByUserID(userid);

                break;

            case "byreimid" : justNeo = REJDBC.findByID(reimid);
                WHAMEN = 1;
                break;
        }
    if (WHAMEN==0) {

        String json = new ObjectMapper().writeValueAsString(reimbursements);
        resp.setContentType("application/json");
        resp.getWriter().write(json);
    }else{
        String json = new ObjectMapper().writeValueAsString(justNeo);
        resp.setContentType("application/json");
        resp.getWriter().write(json);
    }



    }

    //TODO TOP PRIORITY
    //This post requires an emp_id, type, amount, description, is_active, and is_approved
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int emp_id = Integer.parseInt(req.getHeader("emp_id"));
        String type = req.getHeader("type");
        double amount = Double.parseDouble(req.getHeader("amount"));
        String description = req.getHeader("description");
        boolean is_active = Boolean.getBoolean(req.getHeader("is_active"));
        boolean is_approved = Boolean.getBoolean(req.getHeader("is_approved"));;
        REJDBC.Create(ronnie);

    }


    @Override
    public void destroy() {
        super.destroy();
    }


}
