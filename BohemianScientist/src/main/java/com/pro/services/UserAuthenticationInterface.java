package com.pro.services;

import com.pro.models.Employee;

public interface UserAuthenticationInterface {
    public Employee authenticate (String username, String password) ;
}
