package com.pro.services;

import com.pro.data.EmployeeRepository;
import com.pro.data.EmployeeRepositoryJDBC;
import com.pro.models.Employee;

public class UserAuthenticationImpl implements UserAuthenticationInterface {
    EmployeeRepository theRepo;
    Employee user;

    public UserAuthenticationImpl(EmployeeRepository theRepo) {
        this.theRepo = theRepo;
    }

    @Override
    public Employee authenticate(String username, String password) {

        if (theRepo.findByEmail(username)!=null){
            user = theRepo.findByEmail(username);
            if (password.equals(user.getPassword()))
            {
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
