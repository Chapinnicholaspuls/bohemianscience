document.querySelector("#loginbutton").addEventListener("click", evt => {
    evt.preventDefault();
    const email = document.querySelector("#inputEmail").value;
    const pass = document.querySelector("#inputPassword").value;

    const obj = {"Email":email,
        "Pass":pass}

    const json = JSON.stringify(obj);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let content = JSON.parse(this.responseText);
            if (content["responseCode"] === -1){
                document.getElementById("incorrectPH").innerHTML = content["responseContent"];
            }else {
                let employee = JSON.parse(content["responseContent"])
                document.cookie = ("email = " + employee["email"]);
                document.cookie = ("id = " + employee["id"]);
                if (employee["accountType"] === 1) {
                    location.href = "employee/employeeHome.html"
                }else{
                    location.href = "manager/managerHome.html"
                }

            }
        }
    };
    xhttp.open("POST", "http://localhost:8080/project1_war_exploded/login", true);
    xhttp.setRequestHeader("Content-type","application/json")
    xhttp.send(json);
});

