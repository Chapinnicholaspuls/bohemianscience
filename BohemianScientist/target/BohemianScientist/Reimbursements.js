var manager = {
  first_name: "Harry",
  last_name: "Potter",
  id: 1000093,
  email: "harrypotter@hogwarts.org"
};

var employees = [
  {
    first_name: "Trey",
    last_name: "Erb",
    id: 1000103,
    email: "trey_erb@hotmail.com"
  },
  {
    first_name: "Chapin",
    last_name: "Nicholas-Puls",
    id: 1003908,
    email: "chapin@dabbers.com"
  },
  {
    first_name: "Tayler",
    last_name: "Zaskey",
    id: 1000039,
    email: "tayler@email.com"
  },
  {
    first_name: "Harry",
    last_name: "Potter",
    id: 1000093,
    email: "harrypotter@hogwarts.org"
  },
  {
    first_name: "Ron",
    last_name: "Weasly",
    id: 1000233,
    email: "ronweasly@hogwarts.org"
  },
  {
    first_name: "Joe",
    last_name: "Dirt",
    id: 1058473,
    email: "joedirt@dirt.com"
  }

];

var reimbursements = [
  {
    reqdate: "07/21/1995",
    type: "Relocation",
    amount: 75.0,
    is_active: true,
    is_approved: false
  },
  {
    reqdate: "06/28/1998",
    type: "Battle",
    amount: 60.0,
    is_active: true,
    is_approved: false
  },
  {
    reqdate: "03/15/2000",
    type: "Materials",
    amount: 500.0,
    is_active: false,
    is_approved: true
  }
  ,
  {
    reqdate: "06/28/2003",
    type: "Battle",
    amount: 700.0,
    is_active: false,
    is_approved: false
  }
];

var emptable = document.getElementById("employeeTable");
if (emptable) {
  for (i = setEmpTableSize(); i > -1; i--) {
    var x = emptable.insertRow(0);
    x.insertCell(0).innerHTML = employees[i].id;
    x.insertCell(1).innerHTML = employees[i].first_name;
    x.insertCell(2).innerHTML = employees[i].last_name;
  }
}

var emptable = document.getElementById("employees");
if (emptable) {
  for (i = employees.length - 1; i > -1; i--) {
    var x = emptable.insertRow(0);
    x.insertCell(0).innerHTML = employees[i].id;
    x.insertCell(1).innerHTML = employees[i].first_name;
    x.insertCell(2).innerHTML = employees[i].last_name;
    x.insertCell(3).innerHTML = employees[i].email;
  }
}

var reimtable = document.getElementById("reimbursementTable");
if (reimtable) {
  for (i = setReimTableSize(); i > -1; i--) {
    var x = reimtable.insertRow(0);
    x.insertCell(0).innerHTML = reimbursements[i].reqdate;
    x.insertCell(1).innerHTML = reimbursements[i].type;
    x.insertCell(2).innerHTML = "$" + reimbursements[i].amount;
  }
}

var reimtable = document.getElementById("reimbursements");
if (reimtable) {
  for (i = reimbursements.length - 1; i > -1; i--) {
    var x = reimtable.insertRow(0);
    x.insertCell(0).innerHTML = reimbursements[i].reqdate;
    x.insertCell(1).innerHTML = reimbursements[i].type;
    x.insertCell(2).innerHTML = "$" + reimbursements[i].amount;
    if(reimbursements[i].is_active)
        x.insertCell(3).innerHTML = "Pending";
    else{
        if(reimbursements[i].is_approved)
            x.insertCell(3).innerHTML = "Approved";
        else 
            x.insertCell(3).innerHTML = "Denied";
    }
  }
}

var welcome = document.getElementById("welcomeMessage");
if (welcome) {
  welcome.innerHTML =
    "Welcome " + manager.first_name + " " + manager.last_name + "!";
}

function setReimTableSize(){
    if(reimbursements.length > 3)
        return 2;
    else
        return reimbursements.length - 1;
}

function setEmpTableSize(){
    if(employees.length > 3)
        return 2;
    else
        return employees.length - 1;
}
